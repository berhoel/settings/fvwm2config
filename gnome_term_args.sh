#! /bin/bash

# Copyright © 2017 by Berthold Höllmann

# Task  : Generate arguments for gnome-terminal

# Author: Berthold Höllmann <berhoel@gmail.com>

# ID: $Id$
author="$Author$"
date="$Date$"
version="$Revision$"

res=
first=1

profile () {
    if [ $1 = "default" ] ; then
        echo -n
    else
        echo -n "--profile=$1"
    fi
}

for u in "$@" ; do
    if [ $first == 1 ] ; then
        res="--window $(profile $u)"
        first=0
    else
        res="$res --tab $(profile $u)"
    fi
done

echo $res

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "sh tem_args.sh"
# End:
